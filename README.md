# Autoencoding

This project runs MaggotUBA on several datasets and can bundle the trained encoders as pretrained models for LarvaTagger.

The original procedure (on t5+t15) is as follows:
```
# get the source
git clone https://gitlab.com/larvataggerpipelines/Autoencoding
cd Autoencoding
# copy/export data files from hecatonchire to data/raw
make rawdata
# compile training datasets as .hdf5 files
make pretraining_datasets
# train autoencoders and export the encoders as pretrained models for larvatagger
make encoders
```

Note that many datasets and encoders are trained for the purpose of a transfer-learning study. This can be modulated modifying some variables at the top of the Makefile file.

Transfer learning can be performed with the following steps, in continuation of the above-mentioned steps:
```
make training_datasets
make transfer_learning
```

## UAS_Abeta40/42 specific encoder

To train an autoencoder on UAS_Abeta40 and UAS_Abeta42 lines in repository t2:
```
make TRACKER=t2 rawdata
make TRACKER=t2 BIAS=5class ENCODERS_PER_BIAS=1 pretraining_datasets
make TRACKER=t2 BIAS=5class ENCODERS_PER_BIAS=1 encoders
```
