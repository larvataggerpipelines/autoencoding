#!/bin/sh

if sacctmgr --version &> /dev/null; then
  if [ -n "$(sacctmgr list cluster | tail -n+3 | grep maestro)" ]; then
    echo maestro
  else
    hostname
  fi
elif [ -n "$(cat /etc/hostname | grep poincare)" ]; then
  echo poincare
fi

