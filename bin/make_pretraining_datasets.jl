#!/usr/bin/env bash
#=
exec julia --color=no --startup-file=no "$@" "${BASH_SOURCE[0]}"
=#

using Random
using TaggingBackends.LarvaDatasets

tracker = ENV["TRACKER"]
inductive_bias = ENV["INDUCTIVE_BIAS"]
taskid = parse(Int, ENV["SLURM_ARRAY_TASK_ID"])

@info "Environment variables" tracker inductive_bias taskid

inductive_biases = Dict(
	"6class" => ["back", "cast", "hunch", "roll", "run", "stop"],
    "7class" => ["back_large", "cast_large", "hunch_large", "roll_large", "run_large", "stop_large", "small_motion"],
    "12class" => ["back_strong", "back_weak", "cast_strong", "cast_weak", "hunch_strong", "hunch_weak", "roll_strong", "roll_weak", "run_strong", "run_weak", "stop_strong", "stop_weak"],
	"roll" => ["roll", "¬roll"],
	"hunch" => ["hunch", "¬hunch"],
	"6class-partial" => ["back_large", "cast", "hunch_large", "roll_large", "run", "stop_large"],
    "6class-large" => ["back_large", "cast_large", "hunch_large", "roll_large", "run_large", "stop_large"],
	"5class" => ["back", "cast", "hunch", "run", "stop"],
)

seed = 0b11010111001001101001110

labels = inductive_biases[inductive_bias]

@info "Making dataset" tracker inductive_bias labels taskid

# inside the container
h5dir = "/data/interim/$(tracker)/$(inductive_bias)/pretrain-$(taskid)"
mkpath(h5dir)

h5file = if isempty(get(ENV, "LEGACY_INTERFACE", ""))
    datadir = "/data/interim/$(tracker)/**.label"
    write_larva_dataset_hdf5(h5dir, datadir, 20;
        labels=labels,
        frameinterval=0.1,
        balance=true,
        past_future_extensions=true,
        seed=seed+(taskid-1))

else
    files = [joinpath(dir, "trx.mat") for (dir, _, files) in walkdir("/data/raw/$tracker")
        for file in files if file == "groundtruth.label"]
    Random.seed!(seed)
    files = Random.shuffle(files)[1:10000] # does not fit in memory otherwise
    write_larva_dataset_hdf5(h5dir, files, 20;
        labels=labels,
        frameinterval=0.1,
        balance=true,
        distributed_sampling=false,
        seed=seed+(taskid-1))
end

@info "Dataset file written" h5file

