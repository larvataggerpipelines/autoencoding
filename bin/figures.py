from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import os

score = {'pretrained': [], 'latent dim': [], 'tracker': [], 'task': [], 'trial': [], 'sample size': [], 'f1-score': []}

for model in os.scandir('../models'):
    parts = model.name.split('-')
    if parts[0] == 'nostate':
        pretrained = False
        encoder = 1
        parts = parts[1:]
    else:
        #assert parts[1] == '6class'
        pretrained = True
        encoder = int(parts[2])
        parts = parts[3:]
    latent_dim, tracker = parts[0], parts[1]
    task = "-".join(parts[2:-2])
    trial, sample_size = parts[-2], parts[-1]
    latent_dim, trial, sample_size = int(latent_dim), int(trial), int(sample_size)
    if pretrained and 1 < trial:
        continue
    try:
        cm = pd.read_csv(os.path.join(model.path, 'confusion.csv'), header=None).values
    except FileNotFoundError:
        print(f"No confusion matrices found for model {model.name}")
        continue
    precision = np.diag(cm) / cm.sum(axis=0)
    recall = np.diag(cm) / cm.sum(axis=1)
    assert np.all(0 < precision)
    assert np.all(0 < recall)
    f1score = 2 * precision * recall / (precision + recall)
    #
    score['pretrained'].append(pretrained)
    score['latent dim'].append(latent_dim)
    score['tracker'].append(tracker)
    score['task'].append(task)
    score['trial'].append(trial)
    score['sample size'].append(sample_size)
    score['f1-score'].append(np.mean(f1score))

score = pd.DataFrame(score)

print(score)

def plot(tracker='t5', latent_dim=25, task='6class', thicker=False):
    plot_kwargs = {}
    save_kwargs = {}
    if thicker:
        sns.set(font_scale=1.5)
        plot_kwargs['linewidth'] = 3
        save_kwargs['bbox_inches'] = 'tight'
        save_kwargs['dpi'] = 1200
    else:
        sns.set(font_scale=1)
    df = score[np.logical_and(score['tracker']==tracker, score['latent dim']==latent_dim, score['task']==task)]
    sns.lineplot(df, x='sample size', y='f1-score', hue='pretrained', err_style='bars', **plot_kwargs)
    figpath = f'{tracker}-{latent_dim}-{task}.png'
    plt.savefig(figpath, **save_kwargs)
    plt.close()


for tracker in ('t5', 't15'):
    for latent_dim in (25, 50, 100, 200):
        for task in ('6class-partial',):#('6class', '7class', '12class'):
            plot(tracker, latent_dim, task, thicker=True)
