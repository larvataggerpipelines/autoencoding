#!/usr/bin/env bash
#=
exec julia --color=no --startup-file=no "$@" "${BASH_SOURCE[0]}"
=#

using Random
using PlanarLarvae.Dataloaders
using TaggingBackends.LarvaDatasets

tracker = ENV["TRACKER"]
classification_task = ENV["CLASSIFICATION_TASK"]
encoders_per_bias = parse(Int, ENV["ENCODERS_PER_BIAS"])
trials_per_task = parse(Int, ENV["TRIALS_PER_TASK"])
taskid = parse(Int, ENV["SLURM_ARRAY_TASK_ID"])

@info "Environment variables" tracker classification_task taskid

classification_tasks = Dict(
	"6class" => ["back", "cast", "hunch", "roll", "run", "stop"],
    "7class" => ["back_large", "cast_large", "hunch_large", "roll_large", "run_large", "stop_large", "small_motion"],
    "12class" => ["back_strong", "back_weak", "cast_strong", "cast_weak", "hunch_strong", "hunch_weak", "roll_strong", "roll_weak", "run_strong", "run_weak", "stop_strong", "stop_weak"],
	"roll" => ["roll", "¬roll"],
	"hunch" => ["hunch", "¬hunch"],
	"6class-partial" => ["back_large", "cast", "hunch_large", "roll_large", "run", "stop_large"],
    "6class-large" => ["back_large", "cast_large", "hunch_large", "roll_large", "run_large", "stop_large"],
)

seed = 0b11010111001001101001110

labels = classification_tasks[classification_task]

@info "Making dataset" tracker classification_task labels taskid

# inside the container
h5dir = taskid == 0 ? "test" : "train-$(taskid)"
h5dir = "/data/interim/$(tracker)/$(classification_task)/$(h5dir)"
mkpath(h5dir)

datadir = "/data/raw/$(tracker)/**.label"
repository = Dataloaders.Repository(datadir)

Random.seed!(seed)
files = Random.shuffle(Dataloaders.files(repository))

train_dataset = Dataloaders.Repository(repository.root, files[1:1000])
test_dataset = Dataloaders.Repository(repository.root, files[1001:1100])

dataset = taskid == 0 ? test_dataset : train_dataset

if 1 < taskid
    seed += taskid - 1
end

h5file = if isempty(get(ENV, "LEGACY_INTERFACE", ""))
    balance = isempty(get(ENV, "RELAX_BALANCE", ""))
    write_larva_dataset_hdf5(h5dir, dataset, 20;
        labels=labels,
        frameinterval=0.1,
        balance=balance || taskid == 0,
        past_future_extensions=false,
        seed=seed)

else
    dataset = [joinpath(dirname(path), "trx.mat") for path in Dataloaders.filepaths(dataset)]
    write_larva_dataset_hdf5(h5dir, dataset, ceil(Int, 20 / 3);
        labels=labels,
        frameinterval=0.1,
        balance=true,
        distributed_sampling=false,
        seed=seed)
end

@info "Dataset file written" h5file

