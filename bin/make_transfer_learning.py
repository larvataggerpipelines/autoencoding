import os
from glob import glob
import numpy as np
from sklearn import metrics
from taggingbackends.explorer import BackendExplorer
from maggotuba.data.make_dataset import make_dataset
from maggotuba.models.train_model import train_model
from maggotuba.models.predict_model import predict_model

taskid = int(os.getenv('SLURM_ARRAY_TASK_ID', '0'))
workers = int(os.getenv('JULIA_THREADS', '4'))
trackers = os.getenv('TRACKERS', 't15-t5')
inductive_bias = os.getenv('INDUCTIVE_BIAS', 'naive') # 'naive'=no pretraining
encoders_per_bias = int(os.getenv('ENCODERS_PER_BIAS', '1'))
classification_task = os.getenv('CLASSIFICATION_TASK', '6class')
trials_per_task = int(os.getenv('TRIALS_PER_TASK', '1'))
latent_dims = [int(d) for d in os.getenv('LATENT_DIMS', '10').split()]
subset_sizes = [int(s) for s in os.getenv('SUBSET_SIZES', '100000').split()]
window_length = 20
layers = 1

balance = bool(os.getenv('RELAX_BALANCE', ''))

assert 0 < taskid

pretraining_tracker, tracker = trackers.split('-')

print('pretraining_tracker=', pretraining_tracker, 'inductive_bias=', inductive_bias, 'tracker=', tracker, 'classification_task=', classification_task, 'taskid=', taskid, 'latent_dims=', latent_dims, 'subset_sizes=', subset_sizes, flush=True)

latent_dim = latent_dims[(taskid - 1) % len(latent_dims)]
encoder = (taskid - 1) // len(latent_dims) + 1

seed = 20957680321

labels = {
        "6class": ["back", "cast", "hunch", "roll", "run", "stop"],
        "7class": ["back_large", "cast_large", "hunch_large", "roll_large", "run_large", "stop_large", "small_motion"],
        "12class": ["back_strong", "back_weak", "cast_strong", "cast_weak", "hunch_strong", "hunch_weak", "roll_strong", "roll_weak", "run_strong", "run_weak", "stop_strong", "stop_weak"],
        "roll": ["roll", "¬roll"],
        "hunch": ["hunch", "¬hunch"],
        "6class-partial": ["back_large", "cast", "hunch_large", "roll_large", "run", "stop_large"],
        "6class-large": ["back_large", "cast_large", "hunch_large", "roll_large", "run_large", "stop_large"],
        }[classification_task]

test_dataset = glob(f"/data/interim/{tracker}/{classification_task}/test/larva_dataset_*_{window_length}_{window_length}_*.hdf5")[0]

for trial in range(1, 1+trials_per_task):

    training_dataset = glob(f"/data/interim/{tracker}/{classification_task}/train-{trial}/larva_dataset_*_{window_length}_{window_length}_*.hdf5")[0]
    dataset_size = int(training_dataset.split('/')[-1].split('.')[0].split('_')[-1])

    pretrained_model = f"{pretraining_tracker}-{inductive_bias}-{encoder}-{latent_dim}"

    for subset_size in subset_sizes:

        model = f"{pretrained_model}-{tracker}-{classification_task}-{trial}-{subset_size}"
        datashare = subset_size / dataset_size
        backend = BackendExplorer(model_instance=model)

        # train
        backend.reset()
        backend.move_to_raw(training_dataset, copy=False)
        make_dataset(backend, labels_expected=True)
        train_model(backend, layers, pretrained_model_instance=pretrained_model,
                    subsets=(datashare,0,1-datashare), seed=seed+subset_size,
                    balancing_strategy='maggotuba' if balance else 'auto')

        # test
        backend.reset_data()
        backend.move_to_raw(test_dataset, copy=False)
        make_dataset(backend, labels_expected=True)
        predicted, expected = predict_model(backend)

        if isinstance(expected[0], bytes):
            expected = [ label.decode('utf8') for label in expected ]

        cm = metrics.confusion_matrix(expected, predicted, labels=labels)

        with open(f'/app/MaggotUBA/models/{model}/confusion.csv', 'w') as f:
            for row in cm:
                for i, val in enumerate(row):
                    if i>0:
                        f.write(',')
                    f.write(str(val))
                f.write("\n")

