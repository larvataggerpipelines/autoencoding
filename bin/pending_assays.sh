#!/bin/bash

(cd data/raw; find -L $1/ -name trx.mat -print0) | xargs -0 -I % sh -c "if ! [ -f \"data/interim/%\" ]; then echo %; fi"
