import os
import subprocess
import tempfile
from glob import glob

taskid = int(os.getenv('SLURM_ARRAY_TASK_ID', '0'))
workers = int(os.getenv('JULIA_THREADS', '4'))
tracker = os.getenv('TRACKER', 't15')
inductive_bias = os.getenv('INDUCTIVE_BIAS', '6class')
encoders_per_bias = int(os.getenv('ENCODERS_PER_BIAS', '0'))
latent_dims = os.getenv('LATENT_DIMS', '10')

assert 0 < taskid
assert 0 < encoders_per_bias

print('tracker=', tracker, 'inductive_bias=', inductive_bias, 'taskid=', taskid, 'latent_dims=', latent_dims, flush=True)

window_length = 20

maggotuba_window_length = window_length * 3

scaling = {'6class': 50,
           'hunch': 50,
           'roll': 50,
          }.get(inductive_bias, 50)
scaling = 10

_dir = f"/data/interim/{tracker}/{inductive_bias}/pretrain-{taskid}"
_files = glob(os.path.join(_dir, 'larva_dataset_*.hdf5'))
larva_dataset_file = _files[0]

print('larva_dataset_file=', larva_dataset_file, flush=True)

maggotuba_project_name = f"{tracker}-{inductive_bias}-{taskid}"
maggotuba_dir = os.path.abspath('/app/MaggotUBA-core')
larva_dataset = os.path.abspath(larva_dataset_file)
pretrained_model_dir = os.path.abspath(f"/app/MaggotUBA/pretrained_models/{maggotuba_project_name}")

pretrain = f"""
#!/bin/bash
echo $BASH_SOURCE
cd workspace

rm -rf {taskid}
echo "poetry run maggotuba setup workspace {taskid} --len_traj {window_length}"
poetry run maggotuba setup workspace {taskid} --len_traj {window_length}
cd {taskid} || exit 1

ln -s {larva_dataset} .
sed -e 's|"data_dir": ""|"data_dir": "{larva_dataset}"|' -e 's|"optim_iter": 1000|"optim_iter": {1000 * scaling}|' -e 's|"num_workers": 4,|"num_workers": {workers},|' -i config.json
sed -e 's|^  ]$|  ],|' -e 's|\\}}|  "spine_interpolation": "linear",\\n  "frame_interval": 0.1,\\n  "swap_head_tail": false\\n\\}}|' -i config.json

for dim in {latent_dims}; do

sed -e "s/config.json/config-$dim.json/" -e "s/\\"dim_latent\\": 10/\\"dim_latent\\": $dim/" config.json > "config-$dim.json"

rm -rf "training_log/latent-$dim"
echo "poetry run maggotuba model train --name \\"latent-$dim\\" --config \\"config-$dim.json\\""
poetry run maggotuba model train --name "latent-$dim" --config "config-$dim.json"
[ -f "training_log/latent-$dim/best_validated_encoder.pt" ] || exit 1

eval loc="{pretrained_model_dir}-$dim"
mkdir -p "$loc"
sed -e "s|{maggotuba_dir}/||" -e "s|workspace/||g" "training_log/latent-$dim/config.json" > "$loc/autoencoder_config.json"
cp "training_log/latent-$dim/best_validated_encoder.pt" "$loc/"

done
"""
with tempfile.NamedTemporaryFile('w+') as fp:
    fp.write(pretrain)
    fp.flush()
    ret = subprocess.run(['/bin/bash', fp.name], cwd=maggotuba_dir, capture_output=True)
    if ret.stdout:
        print(ret.stdout.decode('utf8'), flush=True)
    if ret.stderr:
        print(ret.stderr.decode('utf8'))

